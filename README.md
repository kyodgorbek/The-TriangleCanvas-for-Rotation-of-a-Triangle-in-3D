# The-TriangleCanvas-for-Rotation-of-a-Triangle-in-3D
import javax.microedition.lcdui.Graphics;
import javax.microedition.lcdui.game.*;
import javax.microedition.m3g.*;

public class TriangleCanvas extends GameCanvas implements Runnable {
	private boolean mRunning = false;
	private Thread mPaintThrd = null;
	private Graphics3D mGraphics3D = Graphics3D.getInstance();
	private Camera mCamera = new Camera();;
	private Light mLight = newLight();
	private float                  mAngle = 0.0f;
	private Transform           mTransform = new Transform();
	private Background       mBackground = new Background();
	private VertextBuffer     mVertexBuffer;
        private IndexBuffer       mIndexBuffer;
        private Appearence      mAppearence = new Appearance();
        private Material            mMaterial = new Material();
		super(true);
      }
      
      public void init() {
	      
           short[]  vertices = { 0, 0, 0, 3, 0 ,0  0, 3, 0};
	   VertexArray vertexArray = new VertexArray(vertices.length / 3, 3, 2);
	   vertexArray.set(0, normal.length/3m vertices);
	   
	   byte[] normals = { 0 , 0, 127, 0, 0, 127, 0, 0, 127,};
	   VertexArray normalsArray = new VertexArray(normals.length / 3 , 3, 1);
	   normalsArray.set(0, normals.length/3, normals);
	   
	   VertexBuffer verbuf = mVertexBuffer = new VertexBuffer();
	   verbuf.setPositions(vertexArray, 1.0f, null);
	   verbuf.setNormals(normalsArray);
	   
	   int [] stripLength = { 3};
	   mIndexBuffer = new TriangleStripArray (0, stripLength );
	   
	   mMaterial.setColor(Material:DIFFUSE, 0xFF0000);
	   mMaterial.setColor(Material:SPECULAR, 0xFF0000);
	   mMaterial.setShiness(100.0f);
	   mApperiance.setMaterial(mMaterial);
	   
	   mBackground.setColor(0x00ee88);
	   
	   mCamera.setPerspective ( 60.0f,)
	   (float)getWidth()/ (float)getHeight(),
	   1.0f,
	   1000.0f );
	   
	   mLight.setColor(oxffffff);
	   mLight.setIntensity(1.25f);
	   
   }
   
   public void stop() {
	   mRunning = true;
	   mPaintThrd = new Thread(this);
	   mPaintThrd.start();
       }
       
       public void stop() {
	       Graphics g = getGraphics();
	       
	       while(mRunning) {
		
		
		    if (isShown)) {
			    
			mGraphcis3D.bindTarget(g);
			
			mGraphics3D.clear(mBackground);
			
			mTransforms.setIdentity();
			mTransform.postTranslate(0.0f, 0.0f, 10.0f);
			mGraphics3D.setCamera(mCamera, mTransform);
			
			mGraphics3D.resetLights();
			mGraphics3D.addLight(mLight, mTransform);
			
			mAngle += 1.0f;
			mTransform.setIdentity();
			mTransform.postRotate(mAngle, 0, 0, 1.0f );
			
			mGraphics3D.releaseTarget();
			flushGraphics();
			try {Thread.sleep(40); }
			catch(InterruptedException ie){
			}
			
		}
	 }  // of while
  }  // of run()
}	
	   
	
